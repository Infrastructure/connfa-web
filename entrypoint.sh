#!/bin/bash

sed -i "s/CONNFA_APP_KEY/${CONNFA_APP_KEY}/g" /app/connfa/.env
sed -i "s/CONNFA_APP_URL/${CONNFA_APP_URL}/g" /app/connfa/.env
sed -i "s/CONNFA_MYSQL_HOST/${CONNFA_MYSQL_HOST}/g" /app/connfa/.env
sed -i "s/CONNFA_MYSQL_DB/${CONNFA_MYSQL_DB}/g" /app/connfa/.env
sed -i "s/CONNFA_MYSQL_USER/${CONNFA_MYSQL_USER}/g" /app/connfa/.env
sed -i "s/CONNFA_MYSQL_PASS/${CONNFA_MYSQL_PASS}/g" /app/connfa/.env
sed -i "s/CONNFA_REDIS_SERVICE_HOST/${REDIS_SERVICE_HOST}/g" /app/connfa/.env
sed -i "s/CONNFA_REDIS_PASSWORD/${CONNFA_REDIS_PASSWORD}/g" /app/connfa/.env

# Needed as REDIS_PORT is also a container env var
unset REDIS_PORT

php artisan migrate
php artisan password:change --name=admin --password="${CONNFA_ADMIN_PASS}"
exec httpd -DFOREGROUND
