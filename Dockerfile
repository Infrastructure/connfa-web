FROM centos/php-71-centos7

USER root
RUN yum -y install git && \
    rm -rf /var/cache/yum/* && yum clean all
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/3c21a2c1affd88dd3fec6251e91a53e440bc2198/web/installer -O - -q | php -- --quiet --install-dir=/usr/bin/ --filename=composer
RUN groupadd connfa -g 1000630000 && \
    useradd connfa -g 1000630000 -u 1000630000 -G apache,root -r -l
RUN install -dm750 -o connfa -g connfa /app

USER connfa
WORKDIR /app
RUN git clone https://github.com/lemberg/connfa-integration-server connfa
RUN cd connfa && composer install --no-interaction --no-suggest --no-dev && composer update --no-interaction --no-suggest --no-dev && composer require predis/predis
COPY env /app/connfa/.env

USER root
RUN rm /etc/httpd/conf.d/welcome.conf
COPY httpd.conf /etc/httpd/conf.d/connfa.conf

WORKDIR /app/connfa

COPY patches /tmp/patches
RUN patch -p0 < /tmp/patches/00_REDIS_PORT.patch

RUN chown -R connfa:connfa /app/connfa && \
    chmod -R 664 /app/connfa && \
    find /app/connfa -type d -exec chmod 775 {} +
 
COPY entrypoint.sh /entrypoint.sh
USER 1000630000
ENTRYPOINT ["/entrypoint.sh"]
